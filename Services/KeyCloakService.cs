﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using hasura_proxy.Extensions;
using hasura_proxy.Model;
using Microsoft.AspNetCore.Http;

namespace hasura_proxy.Services
{
    public class KeyCloakService
    {
        private HttpClient client;
        private IHttpContextAccessor http;

        public KeyCloakService(IHttpContextAccessor http, AppSettings settings)
        {
            this.http = http;
            this.client = new HttpClient();
            this.client.BaseAddress = new Uri(settings.KeyCloakAPIBaseUrl);
            this.client.CopyJwtBearerFromHttpContext(http);
        }

        public Task<User> Me
        {
            get
            {
                return this.User(this.http.HttpContext.User.Claims?.SingleOrDefault(x => x?.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier")?.Value);
            }
        }

        public async Task<IEnumerable<User>> Users()
        {
            return await client.GetObjectAsync<IEnumerable<User>>("users");
        }

        public async Task<User> User(string userId)
        {
            if (string.IsNullOrWhiteSpace(userId))
                throw new Exception("Parameter {userId} must be valid");
            return await client.GetObjectAsync<User>(string.Format("users/{0}", userId));
        }

        public async Task<IEnumerable<Role>> Roles()
        {
            return await client.GetObjectAsync<IEnumerable<Role>>("roles");
        }

        public async Task<IEnumerable<Group>> Groups()
        {
            return await client.GetObjectAsync<IEnumerable<Group>>("groups");
        }

        public async Task<IEnumerable<Group>> UserGroups(string userId)
        {
            if (string.IsNullOrWhiteSpace(userId))
                throw new Exception("Parameter {userId} must be valid");
            return await client.GetObjectAsync<IEnumerable<Group>>(string.Format("users/{0}/groups", userId));
        }

        public async Task<IEnumerable<Role>> UserRoles(string userId)
        {
            if (string.IsNullOrWhiteSpace(userId))
                throw new Exception("Parameter {userId} must be valid");
            return await client.GetObjectAsync<IEnumerable<Role>>(string.Format("users/{0}/role-mappings/realm/composite", userId));
        }

    }
}
