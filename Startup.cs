using System;
using System.Text.Json;
using hasura_proxy.Graph;
using hasura_proxy.Graph.Query;
using hasura_proxy.Services;
using HotChocolate;
using HotChocolate.AspNetCore;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Logging;
using Microsoft.IdentityModel.Tokens;

namespace hasura_proxy
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            Environment = env;
        }

        public IConfiguration Configuration { get; }
        public IWebHostEnvironment Environment { get; }


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var appSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingsSection);
            services.AddSingleton(appSettingsSection.Get<AppSettings>());

            // Get a copy of settings so we can have strongly typed usage
            var settings = appSettingsSection.Get<AppSettings>();

            services.AddControllers();

            services.AddAuthentication(o =>
            {
                o.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                o.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;

            })
            .AddJwtBearer(cfg =>
            {
                // only for testing
                cfg.RequireHttpsMetadata = false;
                cfg.Authority = settings.KeyCloakAuthorityUrl;
                cfg.Audience = settings.KeyCloakAudience;
                cfg.IncludeErrorDetails = true;
                cfg.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateAudience = false,
                    ValidateIssuerSigningKey = true,
                    ValidateIssuer = true,
                    ValidIssuer = settings.KeyCloakAuthorityUrl,
                    ValidateLifetime = true
                };
            });
            services.AddAuthorization();

            services.AddCors(options =>
            {
                options.AddPolicy("AllowMyOrigin",
                  builder =>
                  {
                      builder.WithOrigins(settings.AllowedHosts)
                      .SetIsOriginAllowedToAllowWildcardSubdomains();
                      builder.AllowAnyHeader();
                      builder.WithMethods("GET", "POST", "HEAD", "DELETE", "PUT", "OPTIONS");
                      //builder.AllowCredentials();
                  });
            });


            services.AddHttpContextAccessor();

            // Since we're rendering our Type via a resolver in the Query_Root we need to ensure its registered for Dependancy Injection
            services.AddScoped<KeyCloakService>();
            services.AddScoped<KeyCloakQueryType>();

            // Graph QL
            var schema = SchemaBuilder.New()
                .AddAuthorizeDirectiveType()
                .AddQueryType<QueryType>()
                .AddMutationType<MutationType>()
                .Create();

            // Add Graph Service and register Schema
            services.AddGraphQL(schema);


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                //app.UseDeveloperExceptionPage();
                IdentityModelEventSource.ShowPII = true;
            }

            app.UseHttpsRedirection();

            app.UseAuthentication();

            app.UseCors("AllowMyOrigin");

            app.UseRouting();

            app.UseAuthorization();

            app.UseGraphQL(app.ApplicationServices.GetService<AppSettings>().GraphEndPointPath);

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
