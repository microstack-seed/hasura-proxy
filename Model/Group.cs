﻿using System.Text.Json.Serialization;

namespace hasura_proxy.Model
{
    public class Group
    {
        public Group()
        {
        }

        [JsonPropertyName("id")]
        public string Id { get; set; }
        [JsonPropertyName("name")]
        public string GroupName { get; set; }
    }
}
