﻿using System.Collections.Generic;
using System.Text.Json.Serialization;
namespace hasura_proxy.Model
{
    public class Role
    {
        public Role()
        {
        }

        [JsonPropertyName("id")]
        public string Id { get; set; }
        [JsonPropertyName("name")]
        public string RoleName { get; set; }
        [JsonPropertyName("description")]
        public string RoleDescription { get; set; }
        [JsonPropertyName("composite")]
        public bool Composite { get; set; }
        [JsonPropertyName("clientRole")]
        public bool ClientRole { get; set; }
        [JsonPropertyName("containerId")]
        public string ContainerId { get; set; }
        [JsonPropertyName("attributes")]
        public Dictionary<string, List<string>> Attributes { get; set; }
    }
}
