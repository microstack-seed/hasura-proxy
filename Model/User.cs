﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace hasura_proxy.Model
{
    public class User
    {
        public User()
        {
        }

        [JsonPropertyName("id")]
        public string Id { get; set; }
        [JsonPropertyName("username")]
        public string Username { get; set; }
        [JsonPropertyName("firstName")]
        public string FirstName { get; set; }
        [JsonPropertyName("lastName")]
        public string LastName { get; set; }
        [JsonPropertyName("name")]
        public string FullName { get { return string.Format("{0} {1}", this.FirstName, this.LastName).Trim(); } }
        [JsonPropertyName("email")]
        public string Email { get; set; }
        [JsonPropertyName("emailVerified")]
        public bool EmailVerified { get; set; }
        [JsonPropertyName("createdTimestamp")]
        public long CreatedTimestamp { get; set; }
        [JsonPropertyName("attributes")]
        public Dictionary<string, List<string>> Attributes { get; set; }
        [JsonPropertyName("enabled")]
        public bool Enabled { get; set; }
    }
}
