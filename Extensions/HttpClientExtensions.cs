﻿using System;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;

namespace hasura_proxy.Extensions
{
    public static class HttpClientExtensions
    {
        public static async Task<T> GetObjectAsync<T>(this HttpClient client, string path)
        {
            try
            {
                var streamTask = client.GetStreamAsync(path);
                var result = await JsonSerializer.DeserializeAsync<T>(await streamTask);
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static async Task<TOutput> PostAndRecieveObjectAsync<TInput, TOutput>(this HttpClient client, string path, TInput input)
        {
            try
            {
                var data = JsonSerializer.Serialize(input);
                var resp = await client.PostAsync(path, new StringContent(data, Encoding.UTF8, "application/json"));
                var result = await JsonSerializer.DeserializeAsync<TOutput>(await resp.Content.ReadAsStreamAsync());
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static async Task<bool> PostObjectAsync<TInput>(this HttpClient client, string path, TInput input)
        {
            try
            {
                var data = JsonSerializer.Serialize(input);
                var resp = await client.PostAsync(path, new StringContent(data, Encoding.UTF8, "application/json"));
                var result = resp.IsSuccessStatusCode;
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static void CopyJwtBearerFromHttpContext(this HttpClient client, IHttpContextAccessor httpContextAccessor)
        {
            if (client == null)
                return;

            StringValues jwt = "";
            // We want to take the current context user and pass their permissions on to the KeyCloak API. Where all their permissions are enforced
            httpContextAccessor?.HttpContext?.Request?.Headers?.TryGetValue("Authorization", out jwt);
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", jwt.FirstOrDefault()?.Replace("Bearer ", ""));
        }
    }
}
