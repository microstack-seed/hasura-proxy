﻿using hasura_proxy.Graph.Query;
using HotChocolate.Types;

namespace hasura_proxy.Graph
{
    public class QueryType : ObjectType
    {
        protected override void Configure(IObjectTypeDescriptor descriptor)
        {
            descriptor.Name("query");

            descriptor.Field("keycloak").Type<KeyCloakQueryType>().Authorize().Resolver(ctx => ctx.Service<KeyCloakQueryType>());
        }

        public QueryType()
        {
        }
    }

}
