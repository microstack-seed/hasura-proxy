﻿using System.Collections.Generic;
using HotChocolate.Types;
using hasura_proxy.Model;
using hasura_proxy.Services;

namespace hasura_proxy.Graph.Types
{
    public class UserType : ObjectType<User>
    {
        protected override void Configure(IObjectTypeDescriptor<User> descriptor)
        {
            descriptor.Name("user");
            descriptor.Field(x => x.Id).Type<NonNullType<StringType>>();
            descriptor.Field(x => x.Username).Type<NonNullType<StringType>>();
            descriptor.Field(x => x.Email).Type<NonNullType<StringType>>();
            descriptor.Field(x => x.FullName).Type<NonNullType<StringType>>();
            descriptor.Field(x => x.FirstName).Type<NonNullType<StringType>>();
            descriptor.Field(x => x.LastName).Type<NonNullType<StringType>>();
            descriptor.Field(x => x.EmailVerified).Type<NonNullType<BooleanType>>();
            descriptor.Field(x => x.Enabled).Type<NonNullType<BooleanType>>();
            descriptor.Field(x => x.Attributes);
            descriptor.Field(x => x.CreatedTimestamp).Type<NonNullType<LongType>>();
            descriptor.Field("groups").Type<ListType<GroupType>>().Resolver(ctx => ctx.Service<KeyCloakService>().UserGroups(ctx.Parent<User>().Id));
            descriptor.Field("roles").Type<ListType<RoleType>>().Resolver(ctx => ctx.Service<KeyCloakService>().UserRoles(ctx.Parent<User>().Id));
        }

        public UserType()
        {
        }
    }
}
