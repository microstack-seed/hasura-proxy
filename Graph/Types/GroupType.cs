﻿using System;
using hasura_proxy.Model;
using HotChocolate.Types;

namespace hasura_proxy.Graph.Types
{
    public class GroupType : ObjectType<Group>
    {
        protected override void Configure(IObjectTypeDescriptor<Group> descriptor)
        {
            descriptor.Name("group");
            descriptor.Field(x => x.Id).Type<NonNullType<StringType>>();
            descriptor.Field(x => x.GroupName).Name("name").Type<NonNullType<StringType>>();
        }
        public GroupType()
        {
        }

    }
}
