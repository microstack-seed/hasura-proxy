﻿using System;
using HotChocolate.Types;
using hasura_proxy.Model;

namespace hasura_proxy.Graph.Types
{
    public class RoleType : ObjectType<Role>
    {
        protected override void Configure(IObjectTypeDescriptor<Role> descriptor)
        {
            descriptor.Name("role");
            descriptor.Field(x => x.Id).Type<NonNullType<StringType>>();
            descriptor.Field(x => x.RoleName).Name("name").Type<NonNullType<StringType>>();
            descriptor.Field(x => x.RoleDescription).Name("description").Type<StringType>();
            descriptor.Field(x => x.ClientRole).Name("isClientRole").Type<NonNullType<BooleanType>>();
            descriptor.Field(x => x.Composite).Name("isCompositeRole").Type<NonNullType<BooleanType>>();
            descriptor.Field(x => x.ContainerId).Type<StringType>();
            descriptor.Field(X => X.Attributes);
        }

        //private bool Dictionary<T1, T2>()
        //{
        //    throw new NotImplementedException();
        //}

        public RoleType()
        {
        }
    }
}
