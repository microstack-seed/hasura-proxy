﻿using System;
using HotChocolate.AspNetCore.Authorization;
using HotChocolate.Types;

namespace hasura_proxy.Graph
{
    [Authorize]
    public class MutationType : ObjectType
    {
        protected override void Configure(IObjectTypeDescriptor descriptor)
        {
            descriptor.Name("mutate");
            descriptor.Field("test").Resolver(ctx => "test worked");
        }


        public MutationType()
        {
        }
    }
}
