﻿using hasura_proxy.Graph.Types;
using hasura_proxy.Services;
using HotChocolate.Types;

namespace hasura_proxy.Graph.Query
{
    public class KeyCloakQueryType : ObjectType
    {
        protected override void Configure(IObjectTypeDescriptor descriptor)
        {
            descriptor.Name("keycloak");
            descriptor.Field("users").Type<ListType<UserType>>().Resolver(ctx => ctx.Service<KeyCloakService>().Users());
            descriptor.Field("user").Type<UserType>().Argument("id", x => x.Type<NonNullType<StringType>>()).Resolver(ctx => ctx.Service<KeyCloakService>().User(ctx.Argument<string>("id")));
            descriptor.Field("roles").Type<ListType<RoleType>>().Resolver(ctx => ctx.Service<KeyCloakService>().Roles());
            descriptor.Field("groups").Type<ListType<GroupType>>().Resolver(ctx => ctx.Service<KeyCloakService>().Groups());
            descriptor.Field("userRoles").Argument("userId", x => x.Type<StringType>()).Type<ListType<RoleType>>().Resolver(ctx => ctx.Service<KeyCloakService>().UserRoles(ctx.Argument<string>("userId")));
            descriptor.Field("userGroups").Argument("userId", x => x.Type<StringType>()).Type<ListType<GroupType>>().Resolver(ctx => ctx.Service<KeyCloakService>().UserGroups(ctx.Argument<string>("userId")));
        }


        public KeyCloakQueryType()
        {

        }
    }
}
