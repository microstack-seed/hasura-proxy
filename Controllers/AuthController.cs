﻿using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Text.Json;
using System.IdentityModel.Tokens.Jwt;
using Newtonsoft.Json.Linq;

namespace hasura_proxy.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize]
    public class AuthController : ControllerBase
    {
        private readonly IHttpContextAccessor context;
        private readonly AppSettings settings;

        public AuthController(IHttpContextAccessor httpContext, AppSettings settings)
        {
            context = httpContext;
            this.settings = settings;
        }

        [HttpGet]
        public dynamic Get()
        {
            try
            {
                // We have already validated the token via .net core auth services and the attribute [Authorize] on this class
                var authHeader = context.HttpContext.Request.Headers["Authorization"].ToString();
                // Even though we've authenticated lets still do the proper Bearer check and parse
                if (authHeader.StartsWith("Bearer "))
                {
                    var bearer = authHeader.Substring(7, authHeader.Length - 7);
                    var handler = new JwtSecurityTokenHandler();
                    // Decode token
                    var token = handler.ReadJwtToken(bearer);
                    // Get claim
                    var claim = token.Claims.FirstOrDefault(x => x.Type == "resource_access" && x.ValueType == "JSON")?.Value;
                    // Create default role
                    var roles = new string[] { settings.DefaultRole };
                    // If claim isn't null lets try and parse it
                    if (!string.IsNullOrWhiteSpace(claim))
                    {
                        try
                        {
                            // lets try to deserialize the the claim
                            var parsedClaims = JsonSerializer.Deserialize<Dictionary<string, Dictionary<string, string[]>>>(claim);
                            // Check for the client roles object
                            var parsedRoles = parsedClaims.ContainsKey(settings.KeyCloakAudience) ? parsedClaims[settings.KeyCloakAudience] : new Dictionary<string, string[]>();
                            // Check for roles object
                            roles = parsedRoles.ContainsKey("roles") ? parsedRoles["roles"] : new string[] { settings.DefaultRole };
                        }
                        catch (Exception ex)
                        {
                            // swallow this and log out issue
                            Console.WriteLine(ex);
                        }
                        
                    }
                    // Get Groups from the groups claim location
                    var groups = token.Claims.Where(x => x.Type == "group").Select(s => s.Value)?.ToArray();
                    // Build session values (note: we use a dictionary because anonymous objects cannot use hyphens in their names)
                    var resp = new Dictionary<string, string>()
                    {
                        { "X-Hasura-User-Id",  token.Subject },
                        { "X-Hasura-Realm-Role",  string.Join(",", roles) },
                        { "X-Hasura-Role", roles.First() },
                        { "X-Hasura-Realm-Group", string.Join(",", groups) },
                        { "X-Hasura-Group", groups.FirstOrDefault() ?? "" },
                        { "X-Hasura-Header-Authorization", authHeader }
                    };
                    // We must return a HTTP 200 to approve the authorization request
                    return Ok(resp);
                }

            }
            catch (Exception ex)
            {
                Console.Out.Write(ex);
            }

            return new UnauthorizedAccessException();
        }
    }
}