﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace hasura_proxy.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [AllowAnonymous] // its important we use AllowAnonymous for these. Hasura doesn't send auth header with an action request. This is fine as long as we run this app in private where only the intended audience can reach it.
    public class TestController : ControllerBase
    {
        private readonly IHttpContextAccessor context;
        private readonly AppSettings settings;

        public TestController(IHttpContextAccessor httpContext, AppSettings settings)
        {
            context = httpContext;
            this.settings = settings;
        }

        [HttpPost]
        public dynamic Test()
        {
            return Ok(new { payload = "test worked well!" });
        }
    }
}
