﻿using System;
namespace hasura_proxy
{
    public class AppSettings
    {
        public AppSettings()
        {
        }

        public string KeyCloakAPIBaseUrl { get; set; }
        public string GraphEndPointPath { get; set; }
        public string KeyCloakAuthorityUrl { get; set; }
        public string KeyCloakAudience { get; set; }
        public string AllowedHosts { get; set; }
        
        public string DefaultRole { get; set; }
    }
}
